#Jakob Coles
#January 16, 2018
#Classifies images into one of two arbitrary classes using a k nearest neighbors classifier.
#Change "training_small" to "training" and "test_small" to "test" if you want to use the bigger dataset; it should be 4 times slower.
#Images come from Caltech's machine vision archive <http://www.vision.caltech.edu/html-files/archive.html>
#Requires opencv.
from os import listdir
from cv2 import imread
TYPES = ["leaves", "faces"] #options for classification
K=7
training_data = []
for file in listdir("training_small"):
    #flatten the data into 1D list of color channel values and pair each image with its identification
    training_data += [([int(channel) for row in imread("training_small/"+file) for pixel in row for channel in pixel], TYPES[1] in file)]
    print("Loading training data", file)
accuracy_count = 0.0
testdir = listdir("test_small")
for file in testdir:
    print("Analyzing file", file)
    #load and flatten a test image
    timg = [channel for row in imread("test_small/"+file) for pixel in row for channel in pixel]
    nbrs = []
    #Computes the "distance" between a test image and each image in the training set.
    #Using an n-dimensional space consisting of a series of color channel values in each image.
    for datum in training_data:
        dist = 0
        #Looks at every 1000th channel value. Looking at every value is too slow and redundant.
        for i in range(0, len(datum[0]), 1000):
            dist += abs(datum[0][i] - timg[i]) #Manhattan distance is probably faster than Euclidian.
        nbrs += [(datum, dist)] #associate a neighbor with its distance to the test image
    #First sort the list of neighbors by distance (this is a *nearest* neighbors problem).
    #Then extract the datum, and from there extract the classification. If the sum of the first K classifications
    #is > K/2, then most of the nearest neighbors are TYPES[1], so that's how we classify the test image.
    var = sum(list(map(lambda z:z[1], list(map(lambda y:y[0], sorted(nbrs, key=lambda x:x[1])))))[:K]) > K/2
    accuracy_count += float(var == (TYPES[1] in file))
    print("Identifying", file, "as", TYPES[var])
#I'm aware of the inherent problems with accuracy as a performance metric, but it should be fine here as the classes are of similar size.
print("Accuracy: "+str(float(accuracy_count/float(len(testdir)))*100.0)+"%")
